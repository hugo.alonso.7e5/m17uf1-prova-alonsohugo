using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class puntuacio : MonoBehaviour
{
    public static double punts;
    public GameObject text;
    // Start is called before the first frame update
    void Start()
    {
        punts = 0;
        text = GameObject.Find("puntuacio");
    }

    // Update is called once per frame
    void Update()
    {
        text.GetComponent<Text>().text = punts.ToString();
        Debug.Log(punts);
    }
}
