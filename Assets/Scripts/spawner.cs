using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    private GameObject bigShip;
    private GameObject lilShip;
    public Vector2 movement;

    // Start is called before the first frame update
    private void Start()
    {
        bigShip = GameObject.Find("Alien - Fragate");
        lilShip = GameObject.Find("Alien 4");
        InvokeRepeating("move", 3f, 3f);
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void move()
    {
        movement = new Vector2(Random.Range(-8.5f, 8.6f), 6.5f);
        var randomNum = Random.Range(0, 2);
        if (randomNum == 1)
        {
            Instantiate(bigShip, movement, Quaternion.identity);

        }else if(randomNum == 0)
        {
            Instantiate(lilShip, movement, Quaternion.identity);
        }
    }
}