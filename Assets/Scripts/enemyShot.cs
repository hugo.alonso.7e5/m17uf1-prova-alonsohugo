using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyShot : MonoBehaviour
{
    private GameObject shot;
    private Vector2 point;
    // Start is called before the first frame update
    void Start()
    {
        shot = GameObject.Find("enemyFogo");
        InvokeRepeating("Shoot", 1f, 1f);
    }

    private void Shoot()
    {
        point = new Vector2(transform.position.x, transform.position.y - 3.2f);
        Instantiate(shot, point, Quaternion.identity);
    }
}
