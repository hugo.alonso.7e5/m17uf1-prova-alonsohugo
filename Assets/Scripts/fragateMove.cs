using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fragateMove : MonoBehaviour
{
    public int vida=5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody2D>().gravityScale = 0.2f;
        GetComponent<Rigidbody2D>().velocity = new Vector2(0f,-0.2f);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (vida >= 1)
        {
            vida--;
        }else if(vida == 0)
        {
            puntuacio.punts = puntuacio.punts + 20;
            Destroy(this.gameObject);
        }
    }
}
