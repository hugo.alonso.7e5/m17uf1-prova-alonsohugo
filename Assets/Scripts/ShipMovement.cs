using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{
    public int Speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetAxis("Horizontal") != 0)
        {
            MoverH();
        }
        if (Input.GetAxis("Vertical") != 0)
        {
            MoverV();
        }
    }
    void MoverH()
    {
        transform.position += new Vector3(Input.GetAxis("Horizontal")*Speed*Time.deltaTime,0f,0f);
    }

    void MoverV()
    {
        transform.position += new Vector3(0f, Input.GetAxis("Vertical")*Speed*Time.deltaTime, 0f);
    }
}
