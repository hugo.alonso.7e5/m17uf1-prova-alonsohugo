using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipShots : MonoBehaviour
{
    private GameObject shot;
    private Vector2 point;
    // Start is called before the first frame update
    void Start()
    {
        shot = GameObject.Find("shipFogo");
    }

    // Update is called once per frame
    void Update()
    {
        point = new Vector2(transform.position.x,transform.position.y+1f);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(shot, point, Quaternion.identity);
        }
    }
}
